using System.Collections.Concurrent;
using Microsoft.AspNetCore.Mvc;

namespace LogReplicationServer.Controllers;

[ApiController]
[Route("[controller]")]
public class MasterController : ControllerBase
{
    private readonly ConcurrentBag<string> _masterRepository;
    private readonly ILogger<LogsController> _logger;
    private readonly IConfiguration _configuration;

    public MasterController(
        [FromKeyedServices(nameof(masterRepository))] ConcurrentBag<string> masterRepository,
        ILogger<LogsController> logger,
        IConfiguration configuration)
    {
        _masterRepository = masterRepository;
        _logger = logger;
        _configuration = configuration;
    }

    [HttpPost]
    public void Create([FromBody] string masterUrl)
    {
        if (_configuration["Role"] is null || _configuration["Role"] == "Master") return;

        if (_masterRepository.Any())
        {
            _masterRepository.Clear();
            _masterRepository.Add(masterUrl);
        }
        else
        {
            _masterRepository.Add(masterUrl);
        }

        _logger.LogInformation("Master Url: '{masterUrl}' was saved into memory.", masterUrl);
    }
}