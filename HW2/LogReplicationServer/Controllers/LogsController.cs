using System.Collections.Concurrent;
using LogReplicationServer.Services;
using Microsoft.AspNetCore.Mvc;

namespace LogReplicationServer.Controllers;

[ApiController]
[Route("[controller]")]
public class LogsController : ControllerBase
{
    static int _msgOrderNo = 0;

    private readonly ConcurrentDictionary<int, string> _logsRepository;
    private readonly ConcurrentBag<string> _secondariesRepository;
    private readonly ConcurrentBag<string> _masterRepository;
    private readonly SecondaryApiService _secondaryApiService;
    private readonly ILogger<LogsController> _logger;
    private readonly IConfiguration _configuration;

    public LogsController(
        [FromKeyedServices(nameof(logsRepository))]ConcurrentDictionary<int, string> logsRepository,
        [FromKeyedServices(nameof(secondariesRepository))]ConcurrentBag<string> secondariesRepository,
        [FromKeyedServices(nameof(masterRepository))]ConcurrentBag<string> masterRepository,
        SecondaryApiService secondaryApiService,
        ILogger<LogsController> logger,
        IConfiguration configuration)
    {
        _logsRepository = logsRepository;
        _secondariesRepository = secondariesRepository;
        _masterRepository = masterRepository;
        _secondaryApiService = secondaryApiService;
        _logger = logger;
        _configuration = configuration;
    }

    [HttpGet]
    public Dictionary<int, string> Get()
    {
        return _logsRepository
            .OrderBy(x => x.Key)
            .ToDictionary();
    }

    [HttpGet("shift")]
    public Dictionary<int, string> GetShift([FromQuery] int start, [FromQuery] int end)
    {
        return _logsRepository
            .OrderBy(x => x.Key)
            .Where(x => x.Key >= start && x.Key <= end)
            .ToDictionary();
    }

    [HttpPost("add")]
    public async Task Add([FromBody] string msg, [FromQuery] int writeConcernNo = 1, [FromQuery] bool duplicate = false)
    {
        if (_configuration["Role"] is null || _configuration["Role"] == "Secondary") return;

        var msgOrderNo = Interlocked.Increment(ref _msgOrderNo);

        _logsRepository[msgOrderNo] = msg;
        _logger.LogInformation("Message: '{message}' was saved into memory.", msg);

        var replicateTasks = new List<(Task<HttpResponseMessage> Task, string SecondaryUrl)>();
        foreach (var secondaryUrl in _secondariesRepository.ToArray())
        {
            var replicateTask = _secondaryApiService.ReplicateMessage(secondaryUrl, msgOrderNo, msg);
            replicateTasks.Add((replicateTask, secondaryUrl));

            if (duplicate)
            {
                var replicateTask2 = _secondaryApiService.ReplicateMessage(secondaryUrl, msgOrderNo, msg); // simulate duplicated msgs
            }
        }

        var replicateTasksCopy = replicateTasks.Select(x => x.Task).ToList();
        for (int i = 1; i < writeConcernNo; i++)
        {
            var completedReplicateTask = await Task.WhenAny(replicateTasksCopy);
            replicateTasksCopy.Remove(completedReplicateTask); // Remove the completed task from the list
        }

        foreach (var replicateTask in replicateTasks.Where(x => !replicateTasksCopy.Select(y => y.Id).Contains(x.Task.Id)))
        {
            var responseMessage = await replicateTask.Task;
            if (responseMessage.IsSuccessStatusCode)
            {
                _logger.LogInformation("Message: '{message}' was replicated to Secondary with URL: '{secondaryUrl}'.", msg, replicateTask.SecondaryUrl);
            }
            else
            {
                var responseContent = await responseMessage.Content.ReadAsStringAsync();
                _logger.LogError("Response Status Code: '{statusCode}'. Response Content: '{content}'.", responseMessage.StatusCode, responseContent);
            }
        }
        
        _logger.LogInformation("Message: '{message}' was replicated to {replicatedSecondaryCount} Secondaries.", msg, writeConcernNo - 1);
    }

    [HttpPost("replicate")]
    public async Task Replicate([FromHeader] int msgOrderNo, [FromBody] string msg)
    {
        if (_configuration["Role"] is null || _configuration["Role"] == "Master") return;
        if (_logsRepository.ContainsKey(msgOrderNo)) return;

        await Task.Delay(TimeSpan.FromSeconds(Random.Shared.Next(5, 15)));

        var lastMsgOrderNo = _logsRepository.OrderBy(x => x.Key).Select(x => x.Key).LastOrDefault();
        if (msgOrderNo - lastMsgOrderNo > 1)
        {
            var masterUrl = _masterRepository.First();
            var msgsShiftResult = await _secondaryApiService.GetMessagesShift(masterUrl, lastMsgOrderNo, msgOrderNo);

            foreach (var msgShiftResult in msgsShiftResult)
            {
                _logsRepository[msgShiftResult.Key] = msgShiftResult.Value;
            }
        }
        else
        {
            _logsRepository[msgOrderNo] = msg;
        }

        _logger.LogInformation("Message: '{message}' was replicated into memory.", msg);
    }
}