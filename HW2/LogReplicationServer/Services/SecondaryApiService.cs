using System.Text;
using System.Text.Json;

namespace LogReplicationServer.Services;

public class SecondaryApiService
{
    private readonly HttpClient _httpClient;

    public SecondaryApiService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<HttpResponseMessage> ReplicateMessage(string uri, int msgOrderNo, string msg)
    {
        var stringContent = new StringContent(JsonSerializer.Serialize(msg), Encoding.UTF8, "application/json");
        stringContent.Headers.Add(nameof(msgOrderNo), msgOrderNo.ToString());
        return await _httpClient.PostAsync(uri + "/Logs/replicate", stringContent);
    }

    public async Task<Dictionary<int, string>> GetMessagesShift(string uri, int start, int end)
    { 
        var responseMessage = await _httpClient.GetAsync(uri + $"/Logs/shift?start={start}&end={end}");
        return (await responseMessage.Content.ReadFromJsonAsync<Dictionary<int, string>>())!;
    }
}