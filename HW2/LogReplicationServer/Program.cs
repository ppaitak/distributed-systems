using System.Collections.Concurrent;
using LogReplicationServer.Services;

internal class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.Services.AddKeyedSingleton<ConcurrentDictionary<int, string>>("logsRepository");
        builder.Services.AddKeyedSingleton<ConcurrentBag<string>>("secondariesRepository");
        builder.Services.AddKeyedSingleton<ConcurrentBag<string>>("masterRepository");
        builder.Services.AddHttpClient<SecondaryApiService>();
        builder.Services.AddControllers();
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        var app = builder.Build();
        app.UseSwagger();
        app.UseSwaggerUI();
        // app.UseHttpsRedirection();
        app.UseAuthorization();
        app.MapControllers();
        app.Run();
    }
}