#!/bin/bash

# Define your image name
IMAGE_NAME="ds-hw2"

# Define the Docker network
NETWORK_NAME="local"

# Define the path to your Dockerfile
# Assuming the Dockerfile is in a subdirectory named 'DockerFolder' next to the script
DOCKERFILE_PATH="./LogReplicationServer"

# Step 1: Build the Docker image
docker build -t $IMAGE_NAME -f $DOCKERFILE_PATH/Dockerfile .

# Create the network
docker network create $NETWORK_NAME

# Start the first container with Role=Master
docker run -d -e Role=Master -p 8081:8081 -p 8080:8080 --network local --name aspnetcore_app_master $IMAGE_NAME

# Start two more containers with Role=Secondary
docker run -d -e Role=Secondary -p 8083:8081 -p 8082:8080 --network local --name aspnetcore_app_secondary1 $IMAGE_NAME
docker run -d -e Role=Secondary -p 8085:8081 -p 8084:8080 --network local --name aspnetcore_app_secondary2 $IMAGE_NAME

# Step 2: Keep the window open
echo "Press enter to exit"
docker ps
read