## HW1



## Guideline

1. Run `build-and-run.sh` located in `...\distributed-systems\HW2\build-and-run.sh`.
   ![img.png](img.png)
2. Open swagger ui for each instance in any browser (the same can be done in postman or any other tool):
   * Master - `http://localhost:8080/swagger/index.html`;
   * Secondary1 - `http://localhost:8082/swagger/index.html`;
   * Secondary2 - `http://localhost:8084/swagger/index.html`;
   
   ![img_1.png](img_1.png)
    
   The APIs contracts are the same for each instance:
   * `GET http://localhost:8080/Logs`
   * `GET http://localhost:8080/Logs/shift`
   * `POST http://localhost:8080/Logs/add`
   * `POST http://localhost:8080/Logs/replicate`
   * `POST http://localhost:8080/Master`
   * `POST http://localhost:8080/Secondaries`
3. Add each Secondary url address to Master `POST .../Secondaries` API.
   * `"http://aspnetcore_app_secondary1:8080"`
   * `"http://aspnetcore_app_secondary2:8080"`

   ![img_2.png](img_2.png)
   ![img_3.png](img_3.png)

   #### Note:
   8080 is a default `CONTAINER_PORT` for each instance, and each instance expose its own port mapped to the default `CONTAINER_PORT`, so this is why we pass `:8080` for each secondary instance. While accessing the container from container, Networked service-to-service communication use the `CONTAINER_PORT`.

   ```
    # Start the first container with Role=Master
    docker run -d -e Role=Master -p 8081:8081 -p 8080:8080 --network local --name aspnetcore_app_master $IMAGE_NAME

    # Start two more containers with Role=Secondary
    docker run -d -e Role=Secondary -p 8083:8081 -p 8082:8080 --network local --name aspnetcore_app_secondary1 $IMAGE_NAME
    docker run -d -e Role=Secondary -p 8085:8081 -p 8084:8080 --network local --name aspnetcore_app_secondary2 $IMAGE_NAME
   ```
4. Add Master url address to each Secondary `POST .../Master` API.
   * `"http://aspnetcore_app_master:8080"`

   ![img_4.png](img_4.png)
   ![img_5.png](img_5.png)
5. Add Log message to Master `POST http://localhost:8080/Logs/add` API.
![img_6.png](img_6.png)
6. The result:
   * Master ![img_7.png](img_7.png)
   * Secondary1 ![img_8.png](img_8.png)
   * Secondary2 ![img_9.png](img_9.png)
   * Healthy check is working. ![img_10.png](img_10.png)
7. Test Example:
   * Start M + S1
   ![img_11.png](img_11.png)
   * send (Msg1, W=1) - Ok
   ![img_12.png](img_12.png)
   * send (Msg2, W=2) - Ok
   ![img_13.png](img_13.png)
   * send (Msg3, W=3) - Wait
   ![img_14.png](img_14.png)
   ![img_15.png](img_15.png)
   ![img_16.png](img_16.png)
   * send (Msg4, W=1) - Ok
   ![img_17.png](img_17.png)
   * Start S2
   ![img_18.png](img_18.png)
   * Check messages on S2 - [Msg1, Msg2, Msg3, Msg4]
   ![img_19.png](img_19.png)
   It is 1 secondary.
   ![img_20.png](img_20.png)
   It is 2 secondary.
   In this case it just started I did not add a background worker to get all messages on start, as it wasn't a requirement. So after adding 5 message all messages appears. It appeared on 7 message bec after restart the memory was cleaned and master url too, and I forgot to add it.
   ![img_21.png](img_21.png)
8. Run `shutdown.sh`