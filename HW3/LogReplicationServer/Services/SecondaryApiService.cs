using System.Collections.Concurrent;
using System.Text;
using System.Text.Json;
using Polly;
using Polly.Retry;

namespace LogReplicationServer.Services;

public class SecondaryApiService
{
    private readonly ConcurrentDictionary<string, bool> _secondariesRepository;
    private readonly HttpClient _httpClient;

    public SecondaryApiService(
        [FromKeyedServices(nameof(secondariesRepository))]ConcurrentDictionary<string, bool> secondariesRepository,
        HttpClient httpClient)
    {
        _secondariesRepository = secondariesRepository;
        _httpClient = httpClient;
    }

    public async Task<HttpResponseMessage> ReplicateMessage(string uri, int msgOrderNo, string msg)
    {
        var pipeline = new ResiliencePipelineBuilder()
            .AddRetry(new RetryStrategyOptions
            {
                Delay =TimeSpan.FromSeconds(5),
                MaxRetryAttempts = int.MaxValue
            }) // Add retry using the default options
            .AddTimeout(TimeSpan.FromSeconds(10)) // Add 10 seconds timeout
            .Build(); // Builds the resilience pipeline

        return await pipeline.ExecuteAsync(async token =>
        {
            if (!_secondariesRepository[uri]) // not healthy
            {
                throw new Exception($"The secondary with url: '{uri}' is not healthy.");
            }
            
            var stringContent = new StringContent(JsonSerializer.Serialize(msg), Encoding.UTF8, "application/json");
            stringContent.Headers.Add(nameof(msgOrderNo), msgOrderNo.ToString());
            return await _httpClient.PostAsync(uri + "/Logs/replicate", stringContent, token);
        });
    }

    public async Task<Dictionary<int, string>> GetMessagesShift(string uri, int start, int end)
    { 
        var responseMessage = await _httpClient.GetAsync(uri + $"/Logs/shift?start={start}&end={end}");
        return (await responseMessage.Content.ReadFromJsonAsync<Dictionary<int, string>>())!;
    }

    public async Task<HttpResponseMessage> CheckHealth(string uri)
    { 
        return await _httpClient.GetAsync(uri + "/healthz");
    }
}