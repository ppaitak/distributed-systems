using System.Collections.Concurrent;
using LogReplicationServer.Services;

namespace LogReplicationServer.Workers;

public class SecondaryHealthChecker : BackgroundService
{
    private readonly SecondaryApiService _secondaryApiService;
    private readonly ConcurrentDictionary<string, bool> _secondariesRepository;
    private readonly ILogger<SecondaryHealthChecker> _logger;

    public SecondaryHealthChecker(
        SecondaryApiService secondaryApiService,
        [FromKeyedServices(nameof(secondariesRepository))]ConcurrentDictionary<string, bool> secondariesRepository,
        ILogger<SecondaryHealthChecker> logger)
    {
        _secondaryApiService = secondaryApiService;
        _secondariesRepository = secondariesRepository;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            foreach (var secondary in _secondariesRepository)
            {
                try
                {
                    var response = await _secondaryApiService.CheckHealth(secondary.Key);

                    if (response.IsSuccessStatusCode)
                    {
                        _secondariesRepository[secondary.Key] = true;
                        _logger.LogInformation($"API at {secondary.Key} is healthy!");
                    }
                    else
                    {
                        _secondariesRepository[secondary.Key] = false;
                        _logger.LogInformation($"API at {secondary.Key} is not healthy. Status code: {response.StatusCode}");
                    }
                }
                catch (HttpRequestException ex)
                {
                    // Handle exception if the request fails (e.g., network issue)
                    _logger.LogInformation($"Failed to check API health: {ex.Message}");
                }
            }

            // Wait for a defined interval before checking again (e.g., every 5 minutes)
            await Task.Delay(TimeSpan.FromSeconds(5), stoppingToken);
        }
    }
}