using System.Collections.Concurrent;
using Microsoft.AspNetCore.Mvc;

namespace LogReplicationServer.Controllers;

[ApiController]
[Route("[controller]")]
public class SecondariesController : ControllerBase
{
    private readonly ConcurrentDictionary<string, bool> _secondariesRepository;
    private readonly ILogger<LogsController> _logger;
    private readonly IConfiguration _configuration;

    public SecondariesController(
        [FromKeyedServices(nameof(secondariesRepository))] ConcurrentDictionary<string, bool> secondariesRepository,
        ILogger<LogsController> logger,
        IConfiguration configuration)
    {
        _secondariesRepository = secondariesRepository;
        _logger = logger;
        _configuration = configuration;
    }

    [HttpPost]
    public void Create([FromBody] string secondaryUrl)
    {
        if (_configuration["Role"] is null || _configuration["Role"] == "Secondary") return;

        _secondariesRepository[secondaryUrl] = true;
        _logger.LogInformation("Secondary Url: '{secondaryUrl}' was saved into memory.", secondaryUrl);
    }
}