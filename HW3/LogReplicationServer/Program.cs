using System.Collections.Concurrent;
using LogReplicationServer.Services;
using LogReplicationServer.Workers;

internal class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.Services.AddKeyedSingleton<ConcurrentDictionary<int, string>>("logsRepository");
        builder.Services.AddKeyedSingleton<ConcurrentDictionary<string, bool>>("secondariesRepository");
        builder.Services.AddKeyedSingleton<ConcurrentBag<string>>("masterRepository");
        builder.Services.AddHttpClient<SecondaryApiService>();
        builder.Services.AddControllers();
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();
        builder.Services.AddHealthChecks();
        builder.Services.AddSingleton<IHostedService, SecondaryHealthChecker>();
        builder.Services.AddHostedService<SecondaryHealthChecker>();
        
        var app = builder.Build();
        app.UseSwagger();
        app.UseSwaggerUI();
        // app.UseHttpsRedirection();
        app.UseAuthorization();
        app.MapControllers();
        app.MapHealthChecks("/healthz");
        app.Run();
    }
}