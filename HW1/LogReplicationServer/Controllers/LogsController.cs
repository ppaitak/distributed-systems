using System.Collections.Concurrent;
using LogReplicationServer.Services;
using Microsoft.AspNetCore.Mvc;

namespace LogReplicationServer.Controllers;

[ApiController]
[Route("[controller]")]
public class LogsController : ControllerBase
{
    private readonly ConcurrentBag<string> _logsRepository;
    private readonly ConcurrentBag<string> _secondariesRepository;
    private readonly SecondaryApiService _secondaryApiService;
    private readonly ILogger<LogsController> _logger;
    private readonly IConfiguration _configuration;

    public LogsController(
        [FromKeyedServices(nameof(logsRepository))]ConcurrentBag<string> logsRepository,
        [FromKeyedServices(nameof(secondariesRepository))]ConcurrentBag<string> secondariesRepository,
        SecondaryApiService secondaryApiService,
        ILogger<LogsController> logger,
        IConfiguration configuration)
    {
        _logsRepository = logsRepository;
        _secondariesRepository = secondariesRepository;
        _secondaryApiService = secondaryApiService;
        _logger = logger;
        _configuration = configuration;
    }

    [HttpGet]
    public IEnumerable<string> Get()
    {
        return _logsRepository.ToArray();
    }

    [HttpPost]
    public async Task Add([FromBody] string msg)
    {
        _logsRepository.Add(msg);
        _logger.LogInformation("Message: '{message}' was saved into memory.", msg);

        if (_configuration["Role"] is null || _configuration["Role"] == "Secondary") return;

        foreach (var secondaryUrl in _secondariesRepository.ToArray())
        {
            var responseMessage = await _secondaryApiService.AddMessage(secondaryUrl, msg);
            if (responseMessage.IsSuccessStatusCode)
            {
                _logger.LogInformation("Message: '{message}' was replicated to Secondary with URL: '{secondaryUrl}'.", msg, secondaryUrl);
            }
            else
            {
                var responseContent = await responseMessage.Content.ReadAsStringAsync();
                _logger.LogError("Response Status Code: '{statusCode}'. Response Content: '{content}'.", responseMessage.StatusCode, responseContent);
            }
        } 
        
        _logger.LogInformation("Message: '{message}' was replicated to all Secondaries.", msg);
    }
}