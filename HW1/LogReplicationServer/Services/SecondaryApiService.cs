using System.Text;
using System.Text.Json;

namespace LogReplicationServer.Services;

public class SecondaryApiService
{
    private readonly HttpClient _httpClient;

    public SecondaryApiService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<HttpResponseMessage> AddMessage(string uri, string message)
    {
        var stringContent = new StringContent(JsonSerializer.Serialize(message), Encoding.UTF8, "application/json");
        return await _httpClient.PostAsync(uri + "/Logs", stringContent);
    }
}