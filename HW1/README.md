## HW1



## Guideline

1. Run `build-and-run.sh` located in `...\distributed-systems\HW1\build-and-run.sh`.
2. Open swagger ui for each instance (the same can be done in postman or any other tool):
   1. Master - `http://localhost:8080/swagger/index.html`;
   2. Secondary1 - `http://localhost:8082/swagger/index.html`;
   3. Secondary2 - `http://localhost:8084/swagger/index.html`;
   
   ![img.png](img.png)
    
   The API contracts are the same for each instance:
   1. `GET http://localhost:8080/Logs`
   2. `POST http://localhost:8080/Logs`
   3. `POST http://localhost:8080/Secondaries`
3. Add each Secondary url address to Master `POST .../Secondaries` API.
   1. `"http://aspnetcore_app_secondary1:8080"`
   2. `"http://aspnetcore_app_secondary2:8080"`
   #### Note: 
    8080 is a default `CONTAINER_PORT` for each instance, and each instance expose its own port mapped to the default `CONTAINER_PORT`, so this is why we pass `:8080` for each secondary instance. While accessing the container from container, Networked service-to-service communication use the `CONTAINER_PORT`.
    
   ```
    # Start the first container with Role=Master
    docker run -d -e Role=Master -p 8081:8081 -p 8080:8080 --network local --name aspnetcore_app_master $IMAGE_NAME

    # Start two more containers with Role=Secondary
    docker run -d -e Role=Secondary -p 8083:8081 -p 8082:8080 --network local --name aspnetcore_app_secondary1 $IMAGE_NAME
    docker run -d -e Role=Secondary -p 8085:8081 -p 8084:8080 --network local --name aspnetcore_app_secondary2 $IMAGE_NAME
   ```
4. Add Log message to Master `POST http://localhost:8080/Logs` API.
5. The result:
   1. Master ![img_1.png](img_1.png)
   2. Secondary1 ![img_2.png](img_2.png)
   3. Secondary2 ![img_3.png](img_3.png)

## Additionaly:

![img_4.png](img_4.png)
![img_5.png](img_5.png)
![img_6.png](img_6.png)
![img_7.png](img_7.png)
