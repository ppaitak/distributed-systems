﻿using System.Net.Sockets;
using System.Text;

using TcpClient client = new TcpClient("localhost", 5002);
bool exitRequested = false;

await using NetworkStream stream = client.GetStream();
await using StreamWriter writer = new StreamWriter(stream, Encoding.UTF8) { AutoFlush = true };
using StreamReader reader = new StreamReader(stream, Encoding.UTF8);

// Start a separate task to handle exit command
Task exitTask = Task.Run(() =>
{
    Console.WriteLine("Press any key to exit...");
    Console.ReadKey();
    exitRequested = true;
});

// Continuously send "ping" and display response
while (!exitRequested)
{
    try
    {
        await writer.WriteLineAsync("ping");
        Console.WriteLine("Sent ping.");
        
        string? response = await reader.ReadLineAsync();
        if (response == null) // Server closed the connection
        {
            Console.WriteLine("TCP Server closed the connection.");
            break;
        }
        Console.WriteLine($"Received: {response}");
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        break;
    }

    await Task.Delay(1000); // Delay for 1 second between each ping
}

Console.WriteLine("TCP Client exited.");