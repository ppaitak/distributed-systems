﻿using System.Net;
using System.Net.Sockets;
using System.Text;

TcpListener listener = new TcpListener(IPAddress.Any, 5002);

try
{
    listener.Start();
    Console.WriteLine("TCP Server started on localhost:5002");

    while (true)
    {
        using var client = await listener.AcceptTcpClientAsync();
        Console.WriteLine("TCP Client connected.");

        await using var networkStream = client.GetStream();
        using var reader = new StreamReader(networkStream, Encoding.UTF8);
        await using var writer = new StreamWriter(networkStream, Encoding.UTF8) { AutoFlush = true };

        while (client.Connected)
        {
            string? message = await reader.ReadLineAsync();
            if (string.IsNullOrEmpty(message))
            {
                break; // Client closed the connection
            }

            Console.WriteLine($"Received: {message}");
            await writer.WriteLineAsync("pong");
        }

        Console.WriteLine("TCP Client disconnected.");
    }
}
catch (Exception ex)
{
    Console.WriteLine($"TCP Server error: {ex.Message}");
    // Handle exceptions or stop the server
}
finally
{
    listener.Stop();
}