﻿HttpClient client = new HttpClient();
bool exitRequested = false;

// Start a separate task to handle exit command
Task exitTask = Task.Run(() =>
{
    Console.WriteLine("Press any key to exit...");
    Console.ReadKey();
    exitRequested = true;
});

// Continuously send "ping" and display response
while (!exitRequested)
{
    try
    {
        string response = await client.GetStringAsync("http://localhost:5001/ping");
        Console.WriteLine("Sent ping.");
        Console.WriteLine($"Received: {response}");
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        break;
    }

    await Task.Delay(1000); // Delay for 1 second between each ping
}

Console.WriteLine("HTTP Client exited.");