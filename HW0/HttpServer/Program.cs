﻿using System.Net;
using System.Text;

HttpListener listener = new HttpListener();
listener.Prefixes.Add("http://localhost:5001/");

try
{
    listener.Start();
    Console.WriteLine("HTTP Server started on http://localhost:5001/");

    while (true)
    {
        HttpListenerContext context = await listener.GetContextAsync();
        HttpListenerRequest request = context.Request;
        HttpListenerResponse response = context.Response;

        string responseString = "Invalid request";
        if (request.RawUrl?.EndsWith("/ping") == true)
        {
            responseString = "pong";
        }

        byte[] buffer = Encoding.UTF8.GetBytes(responseString);
        response.ContentLength64 = buffer.Length;

        await using Stream output = response.OutputStream;
        await output.WriteAsync(buffer);
    }
}
catch (Exception ex)
{
    Console.WriteLine($"Error: {ex.Message}");
    // Handle exceptions or stop the server
}
finally
{
    listener.Close();
}